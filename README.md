OAuth2 client implementation to help integrate CAS into PHP Yii1 projects.
`#TOBE-2265-cas-php-client-example-page-fo`

> [See the demo](https://cm2.eregistrations.org/)

### Requirements

* Yii 1.1.9 or above.

This client works easily with the user management module Yii-user`.


### Installation

**1\.** Copy the repository content to your `extensions` directory.

Directory structure example:
```php
/protected/
   extensions/
      hoauth/
         hybridauth/
         messages/
         models/
         views/
         widgets/
         DummyUserIdentity.php
         HOAuthAction.php
         HOAuthAdminAction.php
```


**2\.** Edit your controller source code (eg. `LoginController` class with `actionLogin()` method) to add the `oauth` action:

- Action configuration when yii-user module is used:
```php
class LoginController extends Controller
{
    public function actions()
    {
        return array(
            'oauth' => array(
                'class' => 'ext.hoauth.HOAuthAction',
                'useYiiUser' => true,
                'alwaysCheckPass' => false,
                'model' => 'User', // default of this property is: User
            ),
        );
    }
}
```
- Action configuration when yii-user module is NOT used:

```php
class LoginController extends Controller
{
    public function actions()
    {
        return array(
            'oauth' => array(
                'class' => 'ext.hoauth.HOAuthAction',
                'useYiiUser' => false,
                'alwaysCheckPass' => false,
                'model' => 'User', // default of this property is: User
                // map model attributes to attributes from oauth profile
                // your model attribute => profile attribute
                // the list of avaible attributes is below
                'attributes' => array(
                    'your_email_field' => 'email',
                    'your_firstname_field' => 'firstName',
                    'your_lastname_field' => 'lastName',
                    // you can also specify additional values, 
                    // that will be applied to your model (eg. account activation status)
                    'active' => 1,
                ),
            ),
        );
    }
}
```

**3\.** Add the `findByEmail` method to your user's model class.
`(Not needed if using Yii-user module)`
```php
  /**
   * Returns User model by email
   * 
   * @param string $email 
   * @access public
   * @return User
   */
  public function findByEmail($email)
  {
    return self::model()->findByAttributes(array('email' => $email));
  }
```

**4\.** Add the `hoauth.php` configuration file to `application.config.hoauth`

Directory structure example:
```php
/protected/
   config/
      hoauth.php
```

`hoauth.php` file content:
```php
<?php return [
    // Absolute URL to the oauth action
    "base_url" => "https://your.server.org/user/login/oauth",

    // Identity providers
    "providers" => [
        "CAS" => [
            "enabled" => true,
            "keys" => [
                "id" => "client_id", //CLIENT ID AS CONFIGURED IN CAS
                "secret" => "client_secret",  //CLIENT SECRET AS CONFIGURED IN CAS
            ],
            "endpoints" => [
                "api_base_url" => "https://cas-test.eregistrations.org/cback/v1.0/",
                "authorize_url" => "cas/spa.html#/",
                "token_url" => "access_token",
                "user_info_url" => "user/",
            ],
        ],
    ],
    // if you want to enable logging, set 'debug_mode' to true
    "debug_mode" => true,
    // then provide a writable file by the web server on "debug_file"
    "debug_file" => Yii::app()->getRuntimePath() . DIRECTORY_SEPARATOR . "hoauth.log"
];
```


**5\.** Add `Sign in with CAS widget to your login page view:
```php
<?php $this->widget('ext.hoauth.widgets.HOAuth', array(
    'route' => '/user/login'
)); ?>
```
`route` property points to the controller containing the `oauth` action.


**6\.** Configure your application to automatically load the hoauth classes
        when needed, like:

```php
'import' => array(
    ...
    'ext.hoauth.*',
    'ext.hoauth.hybridauth.Hybrid.*',
),
```


### Sources
This implementation is based on the HybridAuth library. Original code base found here: 
- [HybridAuth library code](https://github.com/hybridauth/hybridauth/tree/v2.10.0)
- [yii-hoauth extension](https://github.com/SleepWalker/hoauth)
- [Library's full documentation](https://hybridauth.github.io/hybridauth/userguide.html)

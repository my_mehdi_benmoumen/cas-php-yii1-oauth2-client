<?php /**
 * @var Controller $this
 * @var HUserInfoForm $form
 */
//$this->layout = '//layouts/centeredBox';
if (property_exists($this, 'hide_menus'))
    $this->hide_menus = true;
?>
<div class="card card-underline contain-md">
    <div class="card-body unrow">
        <?php echo $form->getForm(); ?>
    </div>
</div>
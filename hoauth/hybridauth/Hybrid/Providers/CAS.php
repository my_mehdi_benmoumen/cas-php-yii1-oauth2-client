<?php

/* !
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */

/**
 * Hybrid_Providers_CAS provider adapter based on OAuth2 protocol
 *
 */
class Hybrid_Providers_CAS extends Hybrid_Provider_Model_OAuth2
{

    const STATE_KEY = '1530393013';
    public $state;

    /**
     * {@inheritdoc}
     */
    function initialize()
    {
        parent::initialize();
        // Provider apis end-points
        if (!$this->config["endpoints"]["api_base_url"] || !$this->config["endpoints"]["authorize_url"] || !$this->config["endpoints"]["token_url"]) {
            throw new Exception("Please make sure to provide {$this->providerId} endpoints in hoauth.php config file like following : 'endpoints' => [ 'api_base_url' => '...', 'authorize_url' => '...', 'token_url' => '...', 'user_info_url' => '...', ]", 4);
        }


        $this->api->api_base_url = $this->config["endpoints"]["api_base_url"];
        $this->api->authorize_url = $this->api->api_base_url . $this->config["endpoints"]["authorize_url"];
        $this->api->token_url = $this->api->api_base_url . $this->config["endpoints"]["token_url"];

        $this->state = $this->generateState();
    }

    /**
     * {@inheritdoc}
     * CAS DOC :
     * OAuth2 client sends GET request to CAS's authorisation endpoint, basic authentication with client_id/client_secret is recommended, the query string should contain:
     * client_id, same as registered
     * redirect_uri, same as registered
     * response_type, must be "code" here
     * state - mandatory unique identifier of the current request, this will be returned at the end to client, and client is strongly suggested to verify that the state is same as was originally issued by client
     * scope - optional descriptive parameter
     */
    function loginBegin()
    {
        $extra_parameters = array("state" => $this->state);

        Hybrid_Auth::redirect($this->api->authorizeUrl($extra_parameters));
    }

    /**
     * {@inheritdoc}
     */
    function loginFinish()
    {
        // Fix a strange behavior when some provider call back the endpoint with /index.php?hauth.done={provider}?{args}...
        // here we need to parse $_SERVER[QUERY_STRING]
        $request = array();
        $request_str = '';
        foreach ($_REQUEST as $k => $v) {
            if (strrpos($v, '?')) {
                $v = str_replace("?", "&", $v);
            }
            $request_str .= !empty($request_str) ? '&' : '';
            $request_str .= "$k=$v";
        }
        parse_str($request_str, $request);

        Hybrid_Logger::debug("ORIGINAL REQUEST PARAMS : " . CVarDumper::dumpAsString($_REQUEST));
        Hybrid_Logger::debug("FIXED REQUEST PARAMS : " . CVarDumper::dumpAsString($request));

        $_REQUEST = $request;

        $error = (array_key_exists('error', $request)) ? $request['error'] : "";

        // check for errors
        if ($error) {
            throw new Exception("Authentication failed! {$this->providerId} returned an error: $error", 5);
        }

        // try to authenticate user
        $code = (array_key_exists('code', $request)) ? $request['code'] : "";

        // verify that the state sent by CAS is the same as was originally issued by client
        $state = (array_key_exists('state', $request)) ? $request['state'] : "";

        if (empty($state) || !$this->verifyState($state)) {
            throw new Exception("Provider {$this->providerId} returned an invalid state [{$state}].", 0);
        }

        try {
            $this->authenticate($code);
        } catch (Exception $e) {
            throw new Exception("User profile request failed! {$this->providerId} returned an error: " . $e->getMessage(), 6);
        }

        // check if authenticated
        if (!$this->api->access_token) {
            throw new Exception("Authentication failed! {$this->providerId} returned an invalid access token.", 5);
        }

        // store tokens
        $this->token("access_token", $this->api->access_token);
        $this->token("refresh_token", $this->api->refresh_token);
        $this->token("expires_in", $this->api->access_token_expires_in);
        $this->token("expires_at", $this->api->access_token_expires_at);

        // set user connected locally
        $this->setUserConnected();
    }

    private function authenticate($code)
    {
        $get_params = array();

        $post_params = array(
            "grant_type" => "authorization_code",
            "code" => $code,
            "redirect_uri" => $this->api->redirect_uri,
        );

        $url = $this->api->token_url;
        if (!empty($get_params)) {
            $url = $url . (strpos($url, '?') ? '&' : '?') . http_build_query($get_params);
        }

        $this->api->curl_header = array(
            'Authorization: Basic ' . base64_encode($this->api->client_id . ':' . $this->api->client_secret),
            'Content-Type: application/x-www-form-urlencoded',
            'Accept: application/json',
        );;
        $response = $this->api->post($url, $post_params);


        Hybrid_Logger::debug("HTTP CODE: {$this->api->http_code}");
        Hybrid_Logger::debug("authenticate with url: $url");

        if (!$response || !isset($response->access_token)) {
            throw new Exception("The Authorization Service has return: " . CVarDumper::dumpAsString($response));
        }

        if (isset($response->access_token)) $this->api->access_token = $response->access_token;
        if (isset($response->refresh_token)) $this->api->refresh_token = $response->refresh_token;
        if (isset($response->expires_in)) $this->api->access_token_expires_in = $response->expires_in;

        // calculate the expiration time of the access token
        if (isset($response->expires_in)) {
            $this->api->access_token_expires_at = time() + $response->expires_in;
        }

        Hybrid_Logger::debug("ACCESS TOKEN RECEIVED: {$response->access_token}");
        Hybrid_Logger::debug("REFRESH TOKEN RECEIVED: {$response->refresh_token}");
        Hybrid_Logger::debug("expires_in RECEIVED: {$response->expires_in}");

        return $response;
    }

    /**
     * Load the user profile from the IDp api client
     */
    function getUserProfile()
    {
        // refresh tokens if needed
        $this->refreshToken();

        // Get user profile.
        if (!$this->config["endpoints"]["user_info_url"]) {
            throw new Exception("Please make sure to provide {$this->providerId} user_info_url endpoint in hoauth.php config file.", 4);
        }
        $userinfo_endpoint = $this->api->api_base_url . $this->config["endpoints"]["user_info_url"];

        #Hybrid_Logger::info("Calling USERINFO ENDPOINT : [ $userinfo_endpoint ]");

        $headers = array(
            'Authorization: Bearer ' . $this->api->access_token,
            'Accept: application/json',
        );

        #Hybrid_Logger::info("USER PROFILE REQUEST HEADERS : " . CVarDumper::dumpAsString($headers));

        $this->api->curl_header = $headers;
        $data = $this->api->get($userinfo_endpoint);

        #Hybrid_Logger::info("USER PROFILE RESPONSE : " . CVarDumper::dumpAsString($data));

        if (!isset($data->id)) {
            throw new Exception("User profile request failed! {$this->providerId} returned an invalid response: " . Hybrid_Logger::dumpData($data), 6);
        }

        $username = $first_name = $email = '';

        $properties = $data->properties;
        foreach ($properties as $property) {
            if ($property->name == 'first_name') {
                $first_name = $this->extractUserPropertyValue($property->values);
            } else if ($property->name == 'username') {
                $username = $this->extractUserPropertyValue($property->values, true);
            } else if ($property->name == 'primary_email') {
                $email = $this->extractUserPropertyValue($property->values, true);
            }
        }
        #$roles = $data->roles;
        #$message = $data->message;

        $this->user->profile->identifier = $username;
        $this->user->profile->firstName = $first_name;
        $this->user->profile->lastName = (property_exists($data, 'name')) ? $data->name->value : "";
        $this->user->profile->displayName = implode(' ', [$this->user->profile->firstName, $this->user->profile->lastName]);
        $this->user->profile->email = $email;
        if (property_exists($data, 'verified')) {
            $this->user->profile->emailVerified = is_bool($data->verified) && $data->verified ? $this->user->profile->email : '';
        }

        return $this->user->profile;
    }

    private function generateState()
    {
        return CPasswordHelper::hashPassword(self::STATE_KEY, 5);
    }

    private function verifyState($state)
    {
        return CPasswordHelper::verifyPassword(self::STATE_KEY, $state);
    }

    private function extractUserPropertyValue($values, $only_the_first_value = false)
    {
        $vals = [];
        foreach ($values as $val) {
            $vals[] = $val->value;
            if ($only_the_first_value)
                break;
        }
        return implode(' ', $vals);
    }

}
